package converter

import converter.impl.VeryGoodEncryptor

class StringEncryptor(private val encryptor: VeryGoodEncryptor) {

    fun encrypt(value: String) =
        encryptor.apply(value)
}