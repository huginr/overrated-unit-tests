package converter.impl

class VeryGoodEncryptor {

    fun apply(value: String) = value.reversed()
}