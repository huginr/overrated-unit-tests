package todo.writers

/**
 * Writes todos to a store.
 * Prevents duplicates.
 */
interface TodoWriterNoDuplicates {
    /**
     * Writes a task to the store if it's not duplicate, and returns all stored results
     */
    fun write(doWhat: String): List<String>
}