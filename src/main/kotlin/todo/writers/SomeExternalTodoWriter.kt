package todo.writers

class SomeExternalTodoWriter : TodoWriterNoDuplicates {
    private var todos: Set<String> = setOf()

    override fun write(doWhat: String): List<String> {
        todos = todos + doWhat
        return todos.toList()
    }
}