package todo

import todo.Emotion.ANGRY
import todo.Emotion.HAPPY
import todo.Emotion.HUNGRY
import todo.writers.TodoWriterNoDuplicates

enum class Emotion {
    HAPPY,
    ANGRY,
    HUNGRY
}

class EmotionalTodoListWriter(
    private val todoWriter: TodoWriterNoDuplicates
) {
    private val writtenTasks = mutableSetOf<String>()

    fun addTodoFor(emotion: Emotion) =
        when (emotion) {
            HAPPY -> writeNonDuplicate("Aai de hond")
            ANGRY -> writeNonDuplicate("Schop de stoelpoot")
            HUNGRY -> writeNonDuplicate("Frituur loempia's")
        }

    private fun writeNonDuplicate(task: String): List<String> {
        if (!writtenTasks.contains(task)) {
            writtenTasks.addAll(todoWriter.write(task))
        }
        return writtenTasks.toList()
    }
}