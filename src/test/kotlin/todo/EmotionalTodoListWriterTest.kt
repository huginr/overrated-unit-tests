package todo

import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import todo.Emotion.ANGRY
import todo.Emotion.HAPPY
import todo.Emotion.HUNGRY
import todo.writers.TodoWriterNoDuplicates

@ExtendWith(MockKExtension::class)
internal class EmotionalTodoListWriterTest {

    @MockK
    lateinit var todoWriterMock: TodoWriterNoDuplicates

    @InjectMockKs
    lateinit var writer: EmotionalTodoListWriter

    @BeforeEach
    internal fun setUp() {
        every { todoWriterMock.write("Aai de hond") } returns listOf("Aai de hond")
        every { todoWriterMock.write("Schop de stoelpoot") } returns listOf("Schop de stoelpoot")
        every { todoWriterMock.write("Frituur loempia's") } returns listOf("Frituur loempia's")
    }

    @Test
    internal fun `should add a happy todo`() {
        val todos = writer.addTodoFor(HAPPY)

        assertEquals(listOf("Aai de hond"), todos)
    }

    @Test
    internal fun `should add an angry todo`() {
        val todos = writer.addTodoFor(ANGRY)

        assertEquals(listOf("Schop de stoelpoot"), todos)
    }

    @Test
    internal fun `should add a hungry todo`() {
        val todos = writer.addTodoFor(HUNGRY)

        assertEquals(listOf("Frituur loempia's"), todos)
    }

    @Test
    internal fun `should add multiple todos`() {
        writer.addTodoFor(HUNGRY)
        writer.addTodoFor(ANGRY)
        writer.addTodoFor(HAPPY)

        verify { todoWriterMock.write("Frituur loempia's") }
        verify { todoWriterMock.write("Schop de stoelpoot") }
        verify { todoWriterMock.write("Aai de hond") }
    }

    @Test
    internal fun `should add no duplicate todos`() {
        writer.addTodoFor(HUNGRY)
        writer.addTodoFor(HAPPY)
        writer.addTodoFor(HUNGRY)

        verify(exactly = 1) { todoWriterMock.write("Frituur loempia's") }
        verify(exactly = 1) { todoWriterMock.write("Aai de hond") }
    }
}