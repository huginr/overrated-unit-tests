package todo

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import todo.Emotion.ANGRY
import todo.Emotion.HAPPY
import todo.Emotion.HUNGRY
import todo.writers.SomeExternalTodoWriter

internal class EmotionalTodoListWriterIT {

    lateinit var writer: EmotionalTodoListWriter

    @BeforeEach
    private fun setUp() {
        writer = EmotionalTodoListWriter(SomeExternalTodoWriter())
    }

    @Test
    internal fun `should add a happy todo`() {
        val todos = writer.addTodoFor(HAPPY)

        assertEquals(listOf("Aai de hond"), todos)
    }

    @Test
    internal fun `should add an angry todo`() {
        val todos = writer.addTodoFor(ANGRY)

        assertEquals(listOf("Schop de stoelpoot"), todos)
    }

    @Test
    internal fun `should add a hungry todo`() {
        val todos = writer.addTodoFor(HUNGRY)

        assertEquals(listOf("Frituur loempia's"), todos)
    }

    @Test
    internal fun `should add multiple todos`() {
        writer.addTodoFor(HUNGRY)
        writer.addTodoFor(ANGRY)
        val todos = writer.addTodoFor(HAPPY)

        assertEquals(
            listOf("Frituur loempia's", "Schop de stoelpoot", "Aai de hond"),
            todos
        )
    }

    @Test
    internal fun `should add no duplicate todos`() {
        writer.addTodoFor(HUNGRY)
        writer.addTodoFor(HAPPY)
        val todos = writer.addTodoFor(HUNGRY)

        assertEquals(
            listOf("Frituur loempia's", "Aai de hond"),
            todos
        )
    }
}