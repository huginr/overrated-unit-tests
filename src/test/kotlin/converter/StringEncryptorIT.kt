package converter

import converter.impl.VeryGoodEncryptor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class StringEncryptorIT {

    private val stringEncryptor = StringEncryptor(VeryGoodEncryptor())

    @Test
    internal fun `should encrypt given value`() {
        val originalValue = "original"
        val encryptedValue = "lanigiro"

        val actual = stringEncryptor.encrypt(originalValue)

        assertEquals(encryptedValue, actual)
    }
}