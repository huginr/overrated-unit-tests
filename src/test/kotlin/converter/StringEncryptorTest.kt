package converter

import converter.impl.VeryGoodEncryptor
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class StringEncryptorTest {

    @MockK
    private lateinit var encryptorMock: VeryGoodEncryptor

    @InjectMockKs
    private lateinit var stringEncryptor: StringEncryptor

    @Test
    internal fun `should encrypt given value`() {
        val originalValue = "original"
        val encryptedValue = "567@6oT*'87"

        every { encryptorMock.apply(originalValue) } returns encryptedValue

        val actual = stringEncryptor.encrypt(originalValue)

        assertEquals(encryptedValue, actual)
    }
}